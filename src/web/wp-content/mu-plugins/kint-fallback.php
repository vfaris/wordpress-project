<?php
/**
 * Fallback functions in case Kint is not installed
 */

if ( ! class_exists( 'Kint' ) ) {
	function d( $var ) {
		echo '<pre>';
		var_dump( $var );
		echo '</pre>';
	}

	function dd( $var ) {
		echo '<pre>';
		var_dump( $var );
		echo '</pre>';
		exit;
	}

	function s( $var ) {
		echo esc_html( $var );
	}

	function ss( $var ) {
		die( esc_html( $var ) );
	}
}

WordPress Project
=================

Upon a standard WordPress installation, you have a setup that only has the most
basic settings configured. There is a lot of settings you can tweak to improve
security, performance and/or usability.

Here you can find an example project setup for common use cases.

The configuration can be used to install and update a WordPress site using
[Composer](https://getcomposer.org/). The setup already includes some useful
plugins. Change composer.json to fit your needs. Available plugins for Composer
can be found on [WordPress Packagist](http://wpackagist.org/). All stable releases
from [wordpress.org](https://wordpress.org/) should be available.

To install WordPress, run:
```
composer install
```

To install WordPress without development plugins, run:
```
composer install --no-dev
```

This will install and start to configure WordPress. It will ask for your database credentials
and information for your site. After the install you should have a .env file that contains your
database and WordPress settings.

To log into WordPress go to http://your-domain/wp/wp-login.php

After Composer has finished, WordPress will be available in the 'web' folder.
You can change the path in the composer.json. After the install you can remove
the 'src/web' folder.

If you do not plan to update WordPress with Composer, you can remove the
composer.json and composer.lock files and 'src' folder after the install.

See composer-full.json for a large selection of packages that could be useful.  

If you intend to update WordPress with Composer, add the following lines to the .env file.
This will disable auto-updates and updates from the WordPress admin.

```
WP_AUTO_UPDATE_CORE=0
WP_AUTOMATIC_UPDATER_DISABLED=1
WP_DISALLOW_FILE_MODS=1
```

See .env-reference for available settings / examples.  

To update WordPress, including plugins and themes, run:
```
composer update
```

Or without development plugins:
```
composer update --no-dev
```

You can also use [WP-CLI](http://wp-cli.org/) to update WordPress:
```
vendor/bin/wp core update
vendor/bin/wp core update-db
vendor/bin/wp core language update
vendor/bin/wp plugin update --all
vendor/bin/wp theme update --all
```

Author
------
[Walter Ebert](http://walterebert.com/)

Reference
---------
* [Composer](https://getcomposer.org/)
* [WordPress Packagist](http://wpackagist.org/)
* [Improving WordPress Development Workflow with Composer](http://torquemag.io/improving-wordpress-development-workflow-composer/)
* [Your Guide to Composer in WordPress](http://composer.rarst.net/)
* [Apache documentation](https://httpd.apache.org/docs/2.2/)
* [Apache 2.4 upgrade guide](https://httpd.apache.org/docs/2.4/upgrading.html#run-time)
* [WordPress Codex on .htaccess](https://codex.wordpress.org/htaccess)
* [HTML5 boilerplate Apache configuration](https://github.com/h5bp/server-configs-apache)
* [Ask Apache on .htaccess](http://www.askapache.com/htaccess/htaccess.html)
* [Editing wp-config.php](http://codex.wordpress.org/Editing_wp-config.php)
